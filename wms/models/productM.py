'''
_____________  德数WMS  ________________
  email:yueran@pku.edu.org        
  QQ群号：201541335                
  
'''
from wms import base
from flask_security import  UserMixin

db=base.db

class Product(db.Model, UserMixin):
    id_Pdt = db.Column(db.Integer, primary_key=True)
    num_Pdt = db.Column(db.String(255),nullable=False)
    product = db.Column(db.String(255), nullable=False)
    location = db.Column(db.String(255), nullable=False)
    total_Pdt = db.Column(db.Integer,nullable=False)
    lock_Pdt = db.Column(db.Integer,nullable=False)
    unsold_Pdt = db.Column(db.Integer,nullable=False)
    loss_Pdt = db.Column(db.Integer,nullable=False)
    indate = db.Column(db.DateTime())

    def __str__(self):
        return self.product
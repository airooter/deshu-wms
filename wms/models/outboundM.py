'''
_____________  德数WMS  ________________
  email:yueran@pku.edu.org        
  QQ群号：201541335                
  
'''
from wms import base
from flask_security import  UserMixin

db=base.db

class outBound(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    sn = db.Column(db.String(255),nullable=False)
    total_vl = db.Column(db.String(255), nullable=False)
    total_wt = db.Column(db.String(255),nullable=False)
    provider = db.Column(db.String(255),nullable=False)
    product_num = db.Column(db.Integer,nullable=False)
    creater = db.Column(db.Integer,nullable=False)
    date = db.Column(db.DateTime())

    def __str__(self):
        return self.num_in
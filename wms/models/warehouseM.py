'''
_____________  德数WMS  ________________
  email:yueran@pku.edu.org        
  QQ群号：201541335                
  
'''
from wms import base
from flask_security import  UserMixin

db=base.db

class Warehouse(db.Model, UserMixin):
    id_wh = db.Column(db.Integer, primary_key=True)
    num_wh = db.Column(db.String(255),nullable=False)
    location = db.Column(db.String(255), nullable=False)
    total_wh = db.Column(db.Integer,nullable=False)
    lock_wh = db.Column(db.Integer,nullable=False)
    unsold_wh = db.Column(db.Integer,nullable=False)
    loss_wh = db.Column(db.Integer,nullable=False)
    update = db.Column(db.DateTime())

    def __str__(self):
        return self.product
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_babel import Babel
import flask_admin

app = Flask(__name__)
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)
babel = Babel(app)
app.config['BABEL_DEFAULT_LOCALE'] = 'zh_CN'
app.config['BABEL_DEFAULT_TIMEZONE'] = 'UTC'

admin = flask_admin.Admin(
    app,
    '德数物联',
    base_template='my_master.html',
    template_mode='bootstrap3',
)

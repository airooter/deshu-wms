from wms.views import mymodelview
from wtforms import PasswordField

MyModelView=mymodelview.MyModelView

class UserView(MyModelView):
 
    column_labels = dict(   
    email=u'邮件',
    first_name=u'名字',
    last_name=u'姓氏',
    active=u'激活',
    confirmed_at=u'确认日期',
    password=u'密码'
    )
    column_editable_list = ['email', 'first_name', 'last_name']
    column_searchable_list = column_editable_list
    column_exclude_list = ['password']
    column_details_exclude_list = column_exclude_list
    column_filters = column_editable_list
    form_overrides = {
        'password': PasswordField
    }

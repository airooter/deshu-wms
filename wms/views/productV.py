'''
_____________  德数WMS  ________________
  email:yueran@pku.edu.org        
  QQ群号：201541335                
  
'''
from wms.views import mymodelview
from wtforms import PasswordField

class ProductView(mymodelview.MyModelView):
    
  
    column_labels = dict(   
    num_Pdt=u'商品编号',
    product=u'商品名称',
    location=u'位置',
    total_Pdt=u'总数',
    lock_Pdt=u'锁定数',
    unsold_Pdt=u'待售数',
    loss_Pdt=u'损耗数',
    indate=u'入库时间'
    )
    column_editable_list = ['product', 'location', 'total_Pdt','lock_Pdt','loss_Pdt','indate']
    column_searchable_list = column_editable_list
   
    column_filters = column_editable_list
    form_overrides = {
        'password': PasswordField
    }
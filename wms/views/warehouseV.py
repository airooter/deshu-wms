'''
_____________  德数WMS  ________________
  email:yueran@pku.edu.org        
  QQ群号：201541335                
  
'''
from wms.views import mymodelview
from wtforms import PasswordField

class WarehouseView(mymodelview.MyModelView):
    
  
    column_labels = dict(   
    num_wh=u'库房编号',
    location=u'库房位置',
    total_wh=u'总数',
    lock_wh=u'锁定数',
    unsold_wh=u'满库数',
    loss_wh=u'维护停用数',
    upate=u'更新时间'
    
    )
    column_editable_list = ['num_wh', 'location', 'total_wh','lock_wh','loss_wh','update']
    column_searchable_list = column_editable_list
   
    column_filters = column_editable_list
    form_overrides = {
        'password': PasswordField
    }

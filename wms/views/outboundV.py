
'''
_____________  德数WMS  ________________
  email:yueran@pku.edu.org        
  QQ群号：201541335                
  
'''
from wms.views import mymodelview
from wtforms import PasswordField

class outBoundView(mymodelview.MyModelView):
    
  
    column_labels = dict(   
    sn=u'出库单号',
    total_vl=u'总体积',
    total_wt=u'总重量',
    provider=u'供货商',
    product_num=u'商品种类数',
    creater=u'负责人',
    date=u'出库时间'
    
    )
    column_editable_list = ['sn', 'total_vl', 'total_wt','provider','product_num','creater','date']
    column_searchable_list = column_editable_list
   
    column_filters = column_editable_list
    form_overrides = {
        'password': PasswordField
    }
想进行技术交流的人员可以加入QQ群：201541335

欢迎 star,watching,fork, 欢迎贡献代码和提交问题。

[Wiki中是软件说明文档，包括了开源代码项目生成安装部署等，很多技术和知识不仅应用于本项目，也可以应用到其他开源项目中，wiki文档完善进行中](https://gitee.com/beipingOS/deshu-wms/wikis/Home)


大家有没有发现flask速度一般的问题？后面我们可能会开一个新的更快的项目。

和很多开发WEB程序的研究人员和工程师聊过之后，发现大家不仅需要一个程序的实例，更需要搭建程序的材料，这里的材料包括但不限于Flask及其扩展。大家知道Python是一种胶水语言，可以直接把各种途径获得的代码粘贴在一起，来完成一个程序。综上，我们把项目中用到的大多数函数全部放在代码之中，这样对于Flask入门和开发人员，可以更直观的理解整个框架如何运转，德数WMS是整个代码的例程。

德数WMS
![输入图片说明](deshuWMS/%E5%BA%94%E7%94%A8%E6%88%AA%E5%9B%BE/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202022-12-18%20171512.png)
![输入图片说明](deshuWMS/%E5%BA%94%E7%94%A8%E6%88%AA%E5%9B%BE/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202022-12-12%20214711.png)
德数物联网开源WMS
- 用户注册
- 用户登陆
![输入图片说明](deshuWMS/%E5%BA%94%E7%94%A8%E6%88%AA%E5%9B%BE/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202022-12-14%20105609.png)
- 权限管理
- 仓库管理
- 库存管理

![输入图片说明](deshuWMS/%E5%BA%94%E7%94%A8%E6%88%AA%E5%9B%BE/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202022-12-18%20171450.png)

  - Flask及其扩展
  - SQLite，可以更换其他数据库


### 使用说明


- 虚拟环境
    $ pip install virtualenv
    
    $ virtualenv  venv

    $ source  venv/bin/activate
    
    $venv# deactive （关闭虚拟环境）
    
- 安装
    
    $ pip install -r requirements.txt
    
- 启动
    $ python app.py
    
- 首页地址

    localhost:5000/admin
    

#-------------------------------------------
#############  德数WMS  #################
###   email:yueran@pku.edu.org        ###
###   QQ群号：201541335                ###
#-------------------------------------------
import os
import sys
sys.path.insert(0,'.')
from flask import Flask, url_for, redirect, render_template, session,request, abort
from flask_sqlalchemy import SQLAlchemy
from flask_security import Security
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required, current_user, forms, LoginForm
from flask_security.utils import hash_password as encrypt_password
import flask_admin
from flask_admin.contrib import sqla
from flask_admin import helpers as admin_helpers
from flask_admin import BaseView,expose
from wtforms import PasswordField,StringField, form, fields, validators
from flask_babel import Babel

from wms import base,database
from wms.models import user_role
from wms.views import mymodelview, userV,warehouseV,productV,inboundV,outboundV
from wms.models import productM, warehouseM,inboundM,outboundM
# Create Flask application

User=user_role.User
Role=user_role.Role
db=base.db
app=base.app
admin=base.admin
# Setup Flask-Security
user_datastore = database.user_datastore
security = Security(app, user_datastore)


# Flask views
@app.route('/')
def index():
    return render_template('/index.html')

admin.add_view(userV.UserView(User, db.session, menu_icon_type='fa', menu_icon_value='fa-users', name=u"用户管理"))
admin.add_view(productV.ProductView(productM.Product, db.session, menu_icon_type='fa', menu_icon_value='fa-users', name=u"商品详情"))
admin.add_view(inboundV.inBoundView(inboundM.inBound, db.session, menu_icon_type='fa', menu_icon_value='fa-users', name=u"入库管理"))

admin.add_view(warehouseV.WarehouseView(warehouseM.Warehouse, db.session, menu_icon_type='fa', menu_icon_value='fa-users', name=u"库存详情"))
admin.add_view(outboundV.outBoundView(outboundM.outBound, db.session, menu_icon_type='fa', menu_icon_value='fa-users', name=u"出库管理"))

# Add model views
admin.add_view(mymodelview.MyModelView(Role, db.session, menu_icon_type='fa', menu_icon_value='fa-server', name=u'职能分配'))

# define a context processor for merging flask-admin's template context into the
# flask-security views.

@security.context_processor
def security_context_processor():
    return dict(
        admin_base_template=admin.base_template,
        admin_view=admin.index_view,
        h=admin_helpers,
        get_url=url_for
    )


if __name__ == '__main__':
    with app.app_context():
        # Build a sample db on the fly, if one does not exist yet.
        app_dir = os.path.realpath(os.path.dirname(__file__))
        database_path = os.path.join(app_dir, app.config['DATABASE_FILE'])
        if not os.path.exists(database_path):
            database.build_sample_db()
      
        # Start app
        app.run(debug=True)
